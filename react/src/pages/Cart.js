import { useContext, useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { CartContext } from '../CartContext.js'
import { ToastContainer, toast } from 'react-toastify';


const Cart = () => {
    let total = 0;
    const [QtyIncrease, setQtyIncrease] = useState(false);
    const [products, setProducts] = useState([]);
    const { cart, setCart } = useContext(CartContext);
    //  console.log(cart)
    const history = useHistory();
    // console.log(Object.keys(cart.items))
    useEffect(() => {
        if (!cart?.items) {
            return;
        }
        if (QtyIncrease) {
            return;
        }
        fetch(`http://localhost:5000/api/web/cart/products`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ ids: Object.keys(cart.items) })
        }).then(res => res.json())
            .then(products => {
                console.log(products)
                setProducts(products)
                setQtyIncrease(true)
            })
    }, [cart, QtyIncrease])
    const getQty = (productId) => {

        return cart.items[productId];
    }
    const increments = (ProductId) => {
        const currentQty = cart.items[ProductId];
        const _cart = { ...cart };
        _cart.items[ProductId] = currentQty + 1;
        _cart.totalItems += 1;
        setCart(_cart);

    }
    const decrements = (ProductId) => {


        const currentQty = cart.items[ProductId];
        if (currentQty === 1) {
            return;
        }
        const _cart = { ...cart };
        _cart.items[ProductId] = currentQty - 1;
        _cart.totalItems -= 1;
        setCart(_cart);

    }
    const getSum = (ProductId, price) => {
        const sum = price * getQty(ProductId);
        total += sum;
        return sum;

    }
    const productDelete = (productId) => {
        const _cart = { ...cart };
        const qty = _cart.items[productId];
        delete _cart.items[productId];
        _cart.totalItems -= qty;
        setCart(_cart)
        setProducts(products.filter((product) => product._id !== productId))
        toast("Product Delete Successfully!");
    }
    const orderNow = () => {
        window.alert("Order Placed Successfully!");
        setProducts([]);
        setCart({});
    }
 
    return (
        !products?.length
            ?
            <img className="mx-auto w-1/2 mt-12" src="/img/empty.jpg" alt="empty-cart" />
            :
            <div className="container mx-auto lg:w-1/2 w-full pb-24">
                <h1 className="my-12 font-bold"> Cart items</h1>
                <ul>
                    {products.map(myProduct =>

                        <li className="mb-12" key={myProduct._id} >
                            <div className="flex items-center justify-between">
                                <div className="flex items-center">
                                    <img src={myProduct.image} className="h-16" alt="productImage" />
                                    <span className="font-bold  ml-4 w-48">{myProduct.name}</span>
                                </div>
                                <div>
                                    <button onClick={() => increments(myProduct._id)} className="bg-yellow-500 px-4 py-2 rounded-full leading-none">+</button>
                                    <b className="px-4">{getQty(myProduct.id)}</b>
                                    <button onClick={() => decrements(myProduct._id)} className="bg-yellow-500 px-4 py-2 rounded-full leading-none">-</button>
                                </div>
                                <span>₹ {getSum(myProduct._id, myProduct.price)}</span>

                                <button onClick={() => { productDelete(myProduct._id) }} className="bg-red-500 px-4 py-2 rounded-full leading-none text-white">Delete</button>
                                <ToastContainer />
                            </div>
                        </li>
                    )}
                </ul>
                <hr className="my-6" />
                <div className="text-right">
                    <b> Grand Total:</b> ₹ {total}
                </div>
                <div className="text-right mt-6 flex gap-6 justify-end ">
<Link to="/check-out">
                    <button  className="bg-yellow-500 px-6 py-3 rounded-full leading-none">Order Now</button></Link>
                    <button onClick={orderNow} className="bg-gray-500 px-6 py-3  rounded-full leading-none" onClick={() => { history.goBack() }}>Back</button>
                </div>
            </div>
    )
}

export default Cart

