import React from 'react'
import Products from '../components/Products'
const Home = () => {
   const button = {
      outline: "none",
   }
   return (
      <>
         <div className="hero py-16">
            <div className="container mx-auto flex item-center justify-between">
               <div className="w-1/2  ml-20 mt-20">
                  <h6 className="text-lg"><em> Deals of the Day</em></h6>
                  <h1 className="text-3xl md:text-6xl font-bold">Don't Wait !</h1>
                  <button className="px-8 py-2 rounded-full text-white font-bold mt-4 bg-yellow-500 hover:bg-yellow-700 " style={button}>Order Now</button>
               </div>
               <div className="w-1/2">
                  <img className="w-4/5" src="/img/girl3.jpg" alt="PizzaCutout"></img>
               </div>
            </div>
         </div>
         <div className="bp-24">
            <Products />
         </div>
      </>
   )
}
export default Home;
