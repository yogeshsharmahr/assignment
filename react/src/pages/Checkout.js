import React from 'react'
import CheckoutProduct from '../components/CheckoutProduct'

const Checkout = () => {
    return (
        <div>
            <CheckoutProduct/>
        </div>
    )
}

export default Checkout
