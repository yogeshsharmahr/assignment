import { useState, useEffect, useContext } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { CartContext } from '../CartContext.js'
import { ToastContainer, toast } from 'react-toastify';



const ProductDetail = (props) => {
  const [product, setProduct] = useState({})
  const params = useParams();
  const history = useHistory();
  const [isAdding, setIsAdding] = useState(false)
  const [Size, setSize] = useState([]);

  const { cart, setCart } = useContext(CartContext)


  useEffect(() => {
    fetch(`http://localhost:5000/api/web/products/${params.id}`)
      .then(res => res.json())
      .then(product => {
        // console.log(product)
        setProduct(product)
        setSize(product?.size.split(" "))
      });
  }, [params.id]);

  const addToCart = (event, product) => {
    event.preventDefault();


    let _cart = { ...cart }
    if (!_cart.items) {
      _cart.items = {}
    }
    if (_cart.items[product._id]) {
      _cart.items[product._id] += 1;
    } else {
      _cart.items[product._id] = 1;
    }
    if (!_cart.totalItems) {
      _cart.totalItems = 0;
    }
    _cart.totalItems += 1;
    setCart(_cart)
    setIsAdding(true);
    setTimeout(() => {
      setIsAdding(false);
      toast("Product Added Successfully!");
    }, 1000);


  }


  return (
    <>
      <div className="fixed z-10 inset-0 overflow-y-auto" role="dialog" aria-modal="true">
        <div className="flex min-h-screen text-center md:block md:px-2 lg:px-4" style={{ "font-size": '0px' }}>
          <div className="hidden fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity md:block" aria-hidden="true"></div>
          <span className="hidden md:inline-block md:align-middle md:h-screen" aria-hidden="true">&#8203;</span>
          <div className="flex text-base text-left transform transition w-full md:inline-block md:max-w-2xl md:px-4 md:my-8 md:align-middle lg:max-w-4xl">
            <div className="w-full relative flex items-center bg-white px-4 pt-14 pb-8 overflow-hidden shadow-2xl sm:px-6 sm:pt-8 md:p-6 lg:p-8">
              <button type="button" onClick={() => { history.goBack() }} className="absolute top-4 right-4 text-gray-400 hover:text-gray-500 sm:top-8 sm:right-6 md:top-6 md:right-6 lg:top-8 lg:right-8">
                <span className="sr-only">Close</span>
                <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>
              </button>
              <div className="w-full grid grid-cols-1 gap-y-8 gap-x-6 items-start sm:grid-cols-12 lg:gap-x-8">
                <div className="aspect-w-2 aspect-h-3 rounded-lg bg-gray-100 overflow-hidden sm:col-span-4 lg:col-span-5">
                  <img src={product.image} alt="Two each of gray, white, and black shirts arranged on table." className="object-center object-cover" />
                </div>
                <div className="sm:col-span-8 lg:col-span-7">
                  <h2 className="text-2xl font-extrabold text-gray-900 sm:pr-12">
                    {`${product.name} ...`}
                  </h2>
                  <section aria-labelledby="information-heading" className="mt-2">
                    <h3 id="information-heading" className="sr-only">Product information</h3>

                    <p className="text-2xl text-gray-900">
                      ₹ {product.price}
                    </p>
                  </section>
                  <section aria-labelledby="options-heading" className="mt-10">
                    <h3 id="options-heading" className="sr-only">Product options</h3>
                    <form>
                      <div className="mt-10">
                        <div className="flex items-center justify-between">
                          <h4 className="text-sm text-gray-900 font-medium">Size</h4>
                          <a href="#" className="text-sm font-medium text-indigo-600 hover:text-indigo-500">Size guide</a>
                        </div>
                        <fieldset className="mt-4">
                          <legend className="sr-only">
                            Choose a size
                          </legend>
                          <div className="grid grid-cols-4 gap-4">
                            {Size?.map((size, i) =>
                              <label key={i} className="group relative border rounded-md py-3 px-4 flex items-center justify-center text-sm font-medium uppercase hover:bg-gray-50 focus:outline-none sm:flex-1 bg-white shadow-sm text-gray-900 cursor-pointer">
                                <input type="radio" name="size-choice" value="XXS" className="sr-only" aria-labelledby="size-choice-0-label" />
                                <p id="size-choice-0-label">
                                  {size}
                                </p>
                                <div className="absolute -inset-px rounded-md pointer-events-none" aria-hidden="true"></div>
                              </label>
                            )}
                          </div>
                        </fieldset>
                      </div>
                      <div className="mt-2">
                        <h3 className="font-bold">Description :</h3>
                        <p>{product.description}</p>
                      </div>
                      <button disabled={isAdding} className="bg-yellow-500 py-2 px-8 rounded-full font-bold mt-4 focus:outline-none" onClick={(e) => { addToCart(e, product) }}>{isAdding ? "Product Added" : " Add To Cart"} </button>
                    </form>
                  </section>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


    </>

  )
}

export default ProductDetail

