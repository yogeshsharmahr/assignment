import React from 'react'
import Products from '../components/Products'

const ProductsList = () => {
   
    return (
        <div>
             <Products />
        </div>
    )
}

export default ProductsList
