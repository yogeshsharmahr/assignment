import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';
import Home from './pages/Home';
import ProductsList from './pages/ProductsList';
import Cart from './pages/Cart';
import ProductDetail from './pages/ProductDetail';
import Navigation from './components/Navigation';
import { CartContext } from './CartContext.js'
import 'react-toastify/dist/ReactToastify.css';
import Checkout from './pages/Checkout';



const App = () => {
    const [cart, setCart] = useState({});

    // fetch from local storage 
    useEffect(() => {

        const cart = window.localStorage.getItem("cart")

        setCart(JSON.parse(cart));

    }, []);

    useEffect(() => {
        window.localStorage.setItem('cart', JSON.stringify(cart))

    }, [cart]);

    return (
            <Router>
                <CartContext.Provider value={{ cart, setCart }}>
                    <Navigation />
                    <Switch>
                        <Route path="/" component={Home} exact></Route>
                        <Route path="/products" component={ProductsList} exact></Route>
                        <Route path="/products/:id" component={ProductDetail} ></Route>
                        <Route path="/cart" component={Cart} ></Route>
                        <Route path="/check-out" component={Checkout} ></Route>
                    </Switch>
                </CartContext.Provider>
            </Router>
    )
}
export default App;
