import { Link } from 'react-router-dom';
import { useContext} from 'react'
import {CartContext} from '../CartContext.js'


const Navigation = () => {
const cartStyle ={
    background:"rgb(255 159 0)",
    display:'flex',
    padding:'8px,15px',
    borderRadius:'50px',
    
   
    
}

const {cart} = useContext(CartContext);
const img ={
    height:"25px",
    color:"white",
}


    return (
        <>
               
               <nav className="container mx-auto flex item-center justify-between py-10 ">
                 
                      <Link to="/" >
                      <img style={{height:35}} src="/img/logo.svg" alt="logo" className="ml-24" />
                      </Link>
                       <ul className="flex mr-25 pr-10 pt-10">
                           <li >
                               <Link to="/"  >Home</Link>
                              </li>
                           <li className="ml-6"> <Link to="/products"  >Products</Link></li>
                           <li className="ml-6">
                               <Link to="/cart">
                                   <div style={cartStyle}>
                                       <span className="text-white py-1 px-1 ml-1">{cart?.totalItems ? cart?.totalItems : 0 }</span>
                                    <img className="ml-2 py-1 mr-4 mt-1" style={img}  src="/img/cart-icon.png" alt="cart-icon"/>

                                    </div>
                               </Link>
                           </li>
                       </ul>

               </nav>
        </>
    )
}

export default Navigation
