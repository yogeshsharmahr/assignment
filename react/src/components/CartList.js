



const CartList = (props) => {
 
    const {product} =props
       console.log(product);
    //    const getQty =(productId)=>{
    //        return cart.item[productId];
    //    }
    return (
        <ul>
        <li className="mb-12">
            <div className="flex items-center justify-between">
                <div className="flex items-center">
                    <img src={product.image} className="h-16" alt="productImage"/>
                    <span className="font-bold  ml-4 w-48">{product.title}</span>
                </div>
                <div>
                    <button className="bg-yellow-500 px-4 py-2 rounded-full leading-none">-</button>
                    <b className="px-4">2</b>
                    <button className="bg-yellow-500 px-4 py-2 rounded-full leading-none">+</button>
                </div>
                <span>₹ {product.price}</span>
                <button className="bg-red-500 px-4 py-2 rounded-full leading-none text-white">Delete</button>
            </div>
        </li>
   
    </ul>
    )
}

export default CartList
