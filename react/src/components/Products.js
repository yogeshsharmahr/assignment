import { useState, useEffect } from 'react'
import Product from '../components/Product'


const Products = () => {
  const [products, setProducts] = useState([]);
  useEffect(() => {
    fetch("http://localhost:5000/api/web/products")
      .then(response => response.json())
      .then(products => {
        setProducts(products)
      })
  }, [])

  return (
    <>

      <div className="bg-white">
        <div className="max-w-2xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8">
          <h2 className="text-2xl font-extrabold tracking-tight text-gray-900">Sale Coming Soon!</h2>
          <div className="mt-6 grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
            {products?.map((myProduct, i) => <Product key={i} product={myProduct} />)}
          </div>
        </div>
      </div>
    </>

  )
}

export default Products