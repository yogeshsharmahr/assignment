import { useContext, useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { CartContext } from '../CartContext.js'
import { ToastContainer, toast } from 'react-toastify';

const CheckoutProduct = () => {
    let total = 0;
    const [QtyIncrease, setQtyIncrease] = useState(false);
    const [products, setProducts] = useState([]);
    const { cart, setCart } = useContext(CartContext);
    const history = useHistory();
    const [userDetails, setUserDetails] = useState({
        name: '',
        email: ''
    })

    useEffect(() => {
        if (!cart?.items) {
            return;
        }
        if (QtyIncrease) {
            return;
        }
        fetch(`http://localhost:5000/api/web/cart/products`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ ids: Object.keys(cart.items) })
        }).then(res => res.json())
            .then(products => {
                console.log(products)
                setProducts(products)
                setQtyIncrease(true)
            })
    }, [cart, QtyIncrease])
    const getQty = (productId) => {

        return cart.items[productId];
    }
    const increments = (ProductId) => {
        const currentQty = cart.items[ProductId];
        const _cart = { ...cart };
        _cart.items[ProductId] = currentQty + 1;
        _cart.totalItems += 1;
        setCart(_cart);

    }
    const decrements = (ProductId) => {


        const currentQty = cart.items[ProductId];
        if (currentQty === 1) {
            return;
        }
        const _cart = { ...cart };
        _cart.items[ProductId] = currentQty - 1;
        _cart.totalItems -= 1;
        setCart(_cart);

    }
    const getSum = (ProductId, price) => {
        const sum = price * getQty(ProductId);
        total += sum;
        return sum;

    }
    const productDelete = (productId) => {
        const _cart = { ...cart };
        const qty = _cart.items[productId];
        delete _cart.items[productId];
        _cart.totalItems -= qty;
        setCart(_cart)
        setProducts(products.filter((product) => product._id !== productId))
        toast("Product Delete Successfully!");
    }
    const orderNow = () => {
    
        if(userDetails.email && userDetails.name){
            window.alert("Order Placed Successfully!");
            setProducts([]);
            setCart({});
            history.push('/')
        }else{
            window.alert("Please add user details to place order.");
        }
    }

    const setParam = (key, value) => {
        setUserDetails(prev => {
            return {
                ...prev,
                [key]: value
            }
        })
    }

    return (
        <>

            <div class="fixed inset-0 overflow-hidden" aria-labelledby="slide-over-title" role="dialog" aria-modal="true">
                <div class="absolute inset-0 overflow-hidden checkout-cart">
                    <div class="absolute inset-0" aria-hidden="true">
                        <div className="p-3">
                            <h2 class="text-lg font-medium text-gray-900" id="slide-over-title">User Details</h2>
                        </div>
                        <div className="p-3">
                            <label for="price" class="block text-lg fon-bold text-gray-700">Name</label>
                            <input type="text" name="name" value={userDetails.name} onChange={e => setParam('name', e.target.value)} id="price" class="focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-7 pr-12 lg:text-lg border-gray-300" placeholder="Enter Name" />
                        </div>
                        <div className="p-3">
                            <label for="email" class="block text-lg fon-bold text-gray-700">Email</label>
                            <input type="email" name="email" id="email" value={userDetails.email} onChange={e => setParam('email', e.target.value)} class="focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-7 pr-12 lg:text-lg border-gray-300" placeholder="Enter Email" />
                        </div>
                    </div>




                    <div class="fixed inset-y-0 right-0 pl-10 max-w-full flex">
                        <div class="w-screen max-w-md">
                            <div class="h-full flex flex-col bg-white shadow-xl overflow-y-scroll">
                                <div class="flex-1 py-6 overflow-y-auto px-4 sm:px-6">
                                    <div class="flex items-start justify-between">
                                        <h2 class="text-lg font-medium text-gray-900" id="slide-over-title">
                                            Shopping cart
                                        </h2>
                                        <div class="ml-3 h-7 flex items-center">
                                            <button type="button" class="-m-2 p-2 text-gray-400 hover:text-gray-500" onClick={() => { history.goBack() }}>
                                                <span class="sr-only">Close panel</span>
                                                <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                                                </svg>
                                            </button>
                                        </div>
                                    </div>

                                    <div class="mt-8">
                                        <div class="flow-root">

                                            <ul role="list" class="-my-6 divide-y divide-gray-200">
                                                {products.map(myProduct =>
                                                    <li class="py-6 flex">
                                                        <div class="flex-shrink-0 w-24 h-24 border border-gray-200 rounded-md overflow-hidden">
                                                            <img src={myProduct.image} alt="Salmon orange fabric pouch with match zipper, gray zipper pull, and adjustable hip belt." class="w-full h-full object-center object-cover" />
                                                        </div>

                                                        <div class="ml-4 flex-1 flex flex-col">
                                                            <div>
                                                                <div class="flex justify-between text-base font-medium text-gray-900">
                                                                    <h3>
                                                                        <a href="#">
                                                                            {myProduct.name}
                                                                        </a>
                                                                    </h3>
                                                                    <p class="ml-4">
                                                                        ₹ {getSum(myProduct._id, myProduct.price)}
                                                                    </p>
                                                                </div>
                                                                <p class="mt-1 text-sm text-gray-500">
                                                                    {myProduct.size}
                                                                </p>
                                                            </div>
                                                            <div class="flex-1 flex items-end justify-between text-sm">
                                                                <p class="text-gray-500">
                                                                    Qty {cart.totalItems}
                                                                </p>

                                                                <div class="flex">
                                                                    <button type="button" class="font-medium text-indigo-600 hover:text-indigo-500" onClick={() => { productDelete(myProduct._id) }}>Remove</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                )}
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="border-t border-gray-200 py-6 px-4 sm:px-6">
                                    <div class="flex justify-between text-base font-medium text-gray-900">
                                        <p>Total</p>
                                        <p>₹ {total}</p>
                                    </div>
                                    <div class="mt-6" onClick={orderNow}>
                                        <a href="#" class="flex justify-center items-center px-6 py-3 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-indigo-600 hover:bg-indigo-700" >Place Order</a>
                                    </div>
                                    <div class="mt-6 flex justify-center text-sm text-center text-gray-500" onClick={() => { history.goBack() }}>
                                        <p>
                                            or <button type="button" class="text-indigo-600 font-medium hover:text-indigo-500">Continue Shopping<span aria-hidden="true"> &rarr;</span></button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default CheckoutProduct
