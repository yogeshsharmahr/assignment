import { Link } from 'react-router-dom';
import { useContext,useState } from "react";
import {CartContext} from '../CartContext.js'


const Product = ({product}) => {
  // console.log(product.name)
  // console.log("hello")
  const [isAdding ,setIsAdding]=useState(false)
  const {cart,setCart}= useContext(CartContext)
   
   const img = {
       height:"170px",
   }
   const addToCart =(event,product)=>{
       event.preventDefault();
  

       let _cart = {...cart} 
       if(!_cart.items){
           _cart.items ={}
       }
       if(_cart.items[product._id]){
               _cart.items[product._id]+=1;
       }else{
           _cart.items[product._id]= 1;
       }
       if(!_cart.totalItems){
        _cart.totalItems =0;
       }
       _cart.totalItems +=1;
       setCart(_cart)
       setIsAdding(true);
       setTimeout(()=>{
        setIsAdding(false);
       
       },1000);
     

   }

    return (
   





   <>
    <Link to={`/products/${product._id}`}>
      <div className="group relative">
        <div className="w-full min-h-80 bg-gray-200 aspect-w-1 aspect-h-1 rounded-md overflow-hidden group-hover:opacity-75 lg:h-80 lg:aspect-none">
          <img src={product.image} alt="Front of men&#039;s Basic Tee in black." className="w-full h-full object-center object-cover lg:w-full lg:h-full" />
        </div>
        <div className="mt-4 flex justify-between">
          <div>
            <h3 className="text-sm text-gray-700">
           
                <span aria-hidden="true" className="absolute inset-0"></span>
                {`${product.name.substring(0, 30)} ....`}
            
            </h3>
            <p className="mt-1 text-sm text-gray-500">{product.size}</p>
            <p className="text-sm font-medium text-gray-900">₹ {product.price}</p>
          </div>
          
          <div className=" mt-6 flex justify-between flex-col absolute right-0">
          <button disabled={isAdding} className={`${isAdding?` py-2 px-6 bg-green-500 ease-in duration-200 uppercase rounded-full  border-none  focus:outline-none `:`py-2 px-6 transition ease-in duration-200 uppercase rounded-full  border-none   bg-yellow-500 focus:outline-none`}`} onClick={(e)=>{addToCart(e,product)}}>ADD{isAdding ?"ED":" "}</button>
            </div>
        </div>
        
      </div>
        </Link>
      </>
  
  
    )
}

export default Product
