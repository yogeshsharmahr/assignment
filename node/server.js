import express from 'express';
import  mongoose  from 'mongoose';
import { APP_PORT,DB_URL } from './config';

const app = express();
import routes from './routes';
import path from 'path'
import  cors from 'cors'

// for  json middleware apply
app.use(express.json());

app.use(cors())
// database connection
mongoose.connect(DB_URL,{ useNewUrlParser :true, useUnifiedTopology:true,useCreateIndex:true,useFindAndModify:false });

const db = mongoose.connection;
db.on('error',console.error.bind(console,'Connection error:'));
db.once('open', ()=>{
        console.log("DB Connected...");
});


// create app gloabal root project root
global.appRoot= path.resolve(__dirname);

// for multi part upload middleware apply 
app.use(express.urlencoded({extended:false}));

// call routes 
app.use('/api', routes);

//  allow to exist upload static data
app.use('/uploads',express.static('uploads'))

// start server on port 5000
app.listen(APP_PORT , () => console.log(`Listening on Port ${APP_PORT}.`));
