import express from 'express';
const router =express.Router();
import {WebController} from '../controllers/';

//     products router for user 
router.get('/web/products',WebController.index);
router.get('/web/products/:id',WebController.show);
router.post('/web/cart/products',WebController.cart);

export default router;