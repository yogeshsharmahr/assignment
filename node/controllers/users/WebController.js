import { Product } from "../../models";

import { CustomErrorHandler } from "../../services";

const WebController ={
    async index(req,res,next){
    let documents ;
    try{
        documents = await Product.find().select('-updatedAt -__v').sort({ _id: -1 });

    }catch(err){
        return next(CustomErrorHandler.serverError);

    }
    return res.json(documents)
    },
    async show(req, res,next){
        let document ;
        try{
            document = await Product.findOne({_id:req.params.id}).select('-updatedAt -__v');

        }catch(err){
            return next(CustomErrorHandler.serverError());
        }
        return res.json(document);

    },
    async cart(req,res,next){
        let cart
        try{
           
            cart  =await Product.find( { _id: { $in : req.body.ids }  }).select('-updatedAt -__v').sort({ _id: -1 });
        }catch(err){
            return next(CustomErrorHandler.serverError());
        }
        res.json(cart)

    }


}

export default WebController;